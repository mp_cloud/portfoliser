from setuptools import setup

install_reqs = [
    'matplotlib>=1.4.0',
    'numpy>=1.11.1',
    'pandas>=0.18.1',
]

setup(name='portfoliser',
      version='0.1.5',
      description='Trading results and portfolio analyser',
      url='http://github.com/storborg/funniest',
      author='Sprinto Digital',
      author_email='martin.ptacek@sprinto.eu',
      license='MIT',
      packages=['portfoliser'],
      zip_safe=False,
      install_requires=install_reqs)


'''
 setup(
        name=DISTNAME,
        cmdclass=versioneer.get_cmdclass(),
        version=versioneer.get_version(),
        maintainer=MAINTAINER,
        maintainer_email=MAINTAINER_EMAIL,
        description=DESCRIPTION,
        license=LICENSE,
        url=URL,
        long_description=LONG_DESCRIPTION,
        packages=['pyfolio', 'pyfolio.tests'],
        package_data={'pyfolio': ['data/*.*']},
        classifiers=classifiers,
        install_requires=install_reqs,
        extras_require=extras_reqs,
        tests_require=test_reqs,
        test_suite='nose.collector',
    )
'''