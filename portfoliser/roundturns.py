from .utils import *

# Analysis of trades
class trades:

    def __init__(   self,
                    roundturns = None, # Seznam roundturns (uzavřených obchodů)
                    capital = None,
                    granularity = "D", # Toto v 'trades' nepoužíváme, ale v portfoliu to automaticky přidáváme
                    multiplier = 1,
                    monthly_fee=None
                ):

        

        if type(roundturns) != pd.DataFrame:
            raise Exception('Attempt to create trade class without roundturns data.')
        self.roundturns = validate_roundturns_data( roundturns )

        # Multiply trades
        self.roundturns["quantity"] = self.roundturns.apply(lambda x: x["quantity"] * multiplier,axis=1)
        self.roundturns["profit"] = self.roundturns.apply(lambda x: x["profit"] * multiplier,axis=1)
        self.roundturns["commission"] = self.roundturns.apply(lambda x: x["commission"] * multiplier,axis=1)
        self.roundturns["slippage"] = self.roundturns.apply(lambda x: x["slippage"] * multiplier,axis=1)
        #print(self.roundturns)

        self.roundturns["net_profit"] = self.roundturns["profit"] - self.roundturns["commission"] - self.roundturns["slippage"]
        self.roundturns["duration"] = (self.roundturns["exit_date"] - self.roundturns["entry_date"]).dt.days
        self.roundturns["win_loss"] = self.roundturns["net_profit"] / self.roundturns["net_profit"].abs()
        self.roundturns["consequtive_win_loss"] = self.roundturns["win_loss"] * (self.roundturns["win_loss"].groupby((self.roundturns["win_loss"] != self.roundturns["win_loss"].shift()).cumsum()).cumcount() + 1)
        return


    def summary(self, **kargs):

        columns = [ 
                            {"func":"percent_profitable","args":{}}, 
                            {"func":"percent_unprofitable","args":{}}, 
                            {"func":"number_of_all_trades","args":{}}, 
                            {"func":"number_of_winning_trades","args":{}}, 
                            {"func":"number_of_losing_trades","args":{}}, 
                            {"func":"number_of_even_trades","args":{}}, 
                            {"func":"largest_winning_trade","args":{}},
                            {"func":"largest_losing_trade","args":{}}, 
                            {"func":"average_winning_trade","args":{}}, 
                            {"func":"average_trade","args":{}}, 
                            {"func":"average_losing_trade","args":{}}, 
                            {"func":"average_win_loss_ratio","args":{}},
                            {"func":"average_trade_duration","args":{}},
                            {"func":"minimum_trade_duration","args":{}},
                            {"func":"maximum_trade_duration","args":{}},
                            {"func":"maximum_consequtive_wins","args":{}},
                            {"func":"maximum_consequtive_losses","args":{}},
                            
                        ]
        summary = {}
        for column in columns:
            func = column["func"]
            args = column["args"]
            summary[func] = getattr(self, func)(*args)
        return pd.Series(summary)

    def percent_profitable(self):
        return self.number_of_winning_trades() / self.number_of_all_trades()

    def percent_unprofitable(self):
        return self.number_of_losing_trades() / self.number_of_all_trades()

    def number_of_all_trades(self):
        return self.roundturns.count()["entry_date"]

    def number_of_winning_trades(self):
        return self.roundturns[self.roundturns["net_profit"]>0].count()["entry_date"]
    
    def number_of_losing_trades(self):
        return self.roundturns[self.roundturns["net_profit"]<0].count()["entry_date"]

    def number_of_even_trades(self):
        return self.roundturns[self.roundturns["net_profit"]==0].count()["entry_date"]

    def largest_winning_trade(self):
        return self.roundturns[ self.roundturns["net_profit"]>0 ]["net_profit"].max()

    def largest_losing_trade(self):
        return self.roundturns[ self.roundturns["net_profit"]<0 ]["net_profit"].min()

    def average_trade(self):
        return self.roundturns["net_profit"].mean()
    
    def average_winning_trade(self):
        return self.roundturns[ self.roundturns["net_profit"]>0 ]["net_profit"].mean()
    
    def average_losing_trade(self):
        return self.roundturns[ self.roundturns["net_profit"]<0 ]["net_profit"].mean()

    def average_win_loss_ratio(self):
        return self.average_winning_trade() / -self.average_losing_trade()
    
    def average_trade_duration(self):
        return self.roundturns["duration"].mean()

    def maximum_trade_duration(self):
        return self.roundturns["duration"].max()

    def minimum_trade_duration(self):
        return self.roundturns["duration"].min()

    def maximum_consequtive_wins(self):
        return self.roundturns["consequtive_win_loss"].max()
        
    def maximum_consequtive_losses(self):
        return abs(self.roundturns["consequtive_win_loss"].min())
    