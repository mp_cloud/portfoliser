import matplotlib as mpl

# ---------------------------------------------
# Default settings of Matplotlib
# ---------------------------------------------

# Box around the chart
mpl.rcParams['axes.spines.bottom']=False
mpl.rcParams['axes.spines.top']=False
mpl.rcParams['axes.spines.left']=False
mpl.rcParams['axes.spines.right']=False
# Grid
mpl.rcParams['axes.grid'] = True
mpl.rcParams['axes.grid.axis'] = 'y'
mpl.rcParams['axes.grid.which'] = 'major'
mpl.rcParams['grid.alpha']= 0.05
mpl.rcParams['grid.color'] = 'black'
mpl.rcParams['grid.linestyle'] = '-'
mpl.rcParams['grid.linewidth'] = 1
# Lines
mpl.rcParams['lines.linewidth'] = 1.5
# Fonts
mpl.rcParams['figure.titlesize'] = 28
mpl.rcParams['figure.titleweight'] = 'regular'
mpl.rcParams['text.color'] = '707074'
mpl.rcParams['figure.titlesize'] = 30
#mpl.rcParams['figure.titlesize#'] = 30
# Colors
mpl.rcParams['axes.prop_cycle'] = mpl.cycler('color', ['00BEFF', 'D4CA3A', 'FF6DAE', '67E1B5', 'EBACFA', '9E9E9E', 'F1988E', '5DB15A', 'E28544', '52B8AA'])
# Ticks
mpl.rcParams['xtick.top'] = False
mpl.rcParams['xtick.bottom'] = False
mpl.rcParams['ytick.left'] = False
mpl.rcParams['ytick.right'] = False

mpl.rcParams["axes.formatter.limits"] = (-10, 10)