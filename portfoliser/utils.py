import pandas as pd

# Validation of input roundturns dataset
def validate_roundturns_data(  roundturns ):
    '''
    roundturns = pd.DataFrame():
        - entry_date [datetime]
        - exit_date [datetime]
        - profit [float]
        - slippage [float]
        - commission [float]
        - strategy [string]
        - market [string]
        - quantity [int] or [dict]
        - direction [dict]
    '''
    
    # Is it pd.DataFrame?
    if type(roundturns)!=pd.core.frame.DataFrame:
        raise Exception('Parameter "roundturns" has to be a Pandas DataFrame. Provided value was "{}"'.format(type(roundturns)))
    
    # Are there all the mandatory columns with proper data types?
    mandatory_columns = [
                            {"name":"entry_date","dtype":"datetime64[ns]"},
                            {"name":"exit_date","dtype":"datetime64[ns]"},
                            {"name":"profit","dtype":float},
                            {"name":"slippage","dtype":float},
                            {"name":"commission","dtype":float},
                            {"name":"strategy","dtype":object},
                            {"name":"market","dtype":object},
                            {"name":"quantity","dtype":int},
                        ]
    
    # Conversion of columns to the right datatype
    if type(roundturns["entry_date"])!="datetime64[ns]":
        roundturns["entry_date"] = pd.to_datetime(roundturns['entry_date'], format='%Y-%m-%d')
    if type(roundturns["exit_date"])!="datetime64[ns]":
        roundturns["exit_date"] = pd.to_datetime(roundturns['exit_date'], format='%Y-%m-%d')
    if type(roundturns["profit"])!=float:
        roundturns["profit"] = roundturns['profit'].astype(float)
    if type(roundturns["slippage"])!=float:
        roundturns["slippage"] = roundturns['slippage'].astype(float)
    if type(roundturns["strategy"])!=object:
        roundturns["strategy"] = roundturns['strategy'].astype(object)
    if type(roundturns["market"])!=object:
        roundturns["market"] = roundturns['market'].astype(object)
    if type(roundturns["commission"])!=float:
        roundturns["commission"] = roundturns['commission'].astype(float)

    # NaN values detection
    if roundturns.isnull().values.any():
        raise Exception("Sessions data contain NaN values.")

    for mandatory_column in mandatory_columns:
        if mandatory_column["name"] not in list(roundturns.columns):
            raise Exception("The column '{}' is missing in session input DataFrame.".format(mandatory_column["name"]))
        
        if mandatory_column["dtype"] != roundturns[ mandatory_column["name"] ].dtypes:
            raise Exception("The column '{}' has wrong data type '{}'. Expected data type: '{}'.".format(mandatory_column["name"],roundturns[ mandatory_column["name"] ].dtypes,mandatory_column["dtype"]))

    # Adjusting some columns
    roundturns["slippage"] = roundturns[["slippage"]].abs()
    roundturns["commission"] = roundturns["commission"].abs()

    # Sorting by the exit date
    roundturns = roundturns.sort_values("exit_date", ascending=True)

    return roundturns[[ col["name"] for col in mandatory_columns ]]

# Napr. quantity, nebo monthly fee
def validate_strategy_parameters(roundturns,param,varname):
    roundturns_strategy_list = list(roundturns["strategy"].unique())

    # Validation of 'multiplier'
    if isinstance(param, dict):
        parameter_strategy_list = list(param.keys())
        # There cannot be undefined strategy, therefore we check, if we define all the srategies
        if set(roundturns_strategy_list).intersection(parameter_strategy_list)==set(roundturns_strategy_list):
            return param
        else:
            raise Exception("Strategy parameter '{}' misses some strategy. Trading data include these strategies: {}. Parameter contain these strategies: {}".format(varname,roundturns_strategy_list, parameter_strategy_list))
    elif isinstance(param,int):
        return { strategy:param for strategy in roundturns_strategy_list }
    else:
        raise Exception("Parameter '{}' has wrong datatype {}. Expected datatype are int or dict.".format(varname,type(param)))