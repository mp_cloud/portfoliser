import pandas as pd
pd.options.mode.chained_assignment = None
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as mtick
from .utils import *
from datetime import datetime, timedelta
import dateutil.relativedelta

'''
qid	(string)
entry_date (datetime)
exit_date (datetime)
market (string)
entry_pts (float)
exit_pts (float)
net_pts (float)
profit (float)
commission (float)
quantity (float)
mode (strong) ["live"|"test"]
slippage (float)
'''

# Do této funkce se posílá seznam obchodů.
# Důležitý je datum ukončení obchodu 'exit_date' a 'profit'.
# Tato zřída zkoumá výsledky obchodního systému, bez position sizingu
# Pozor! Tato třída nemusí nutně reprezentovat jeden systém. 
class returns():

    def __init__(   self,
                    roundturns = None, # Seznam roundturns (uzavřených obchodů)
                    capital = 100000,
                    monthly_fee = 0,
                    days_back = None,
                    multiplier = 1,
                    #first_date = None,
                    #last_date = None,
                    title = ""): # Přiděláme tu ještě datum odkdy se obcohoduje naostro?
        if type(roundturns) != pd.DataFrame:
            raise Exception('Attempt to create trade class without roundturns data.')
        self.roundturns = validate_roundturns_data( roundturns )
        self.capital = int(capital)
        self.multiplier = int(multiplier)
        self.monthly_fee = int(monthly_fee)
        self.title = title
        
        # Založení temporary tabulek, ve kterých se vše počítá
        self.base_table_daily = None
        self.base_table_weekly = None
        self.base_table_monthly = None
        self.base_table_yearly = None
        self.base_table_trades = None

        if days_back != None:
            self.days_back = days_back
            d = datetime.today() - timedelta(days=self.days_back)
            self.roundturns = self.roundturns[ self.roundturns["exit_date"] >= d ]

        # Multiply trades
        self.roundturns["quantity"] = self.roundturns.apply(lambda x: x["quantity"] * multiplier,axis=1)
        self.roundturns["profit"] = self.roundturns.apply(lambda x: x["profit"] * multiplier,axis=1)
        self.roundturns["commission"] = self.roundturns.apply(lambda x: x["commission"] * multiplier,axis=1)
        self.roundturns["slippage"] = self.roundturns.apply(lambda x: x["slippage"] * multiplier,axis=1)
        #print(self.roundturns)
        return

    # Obecná funkce na vrácení temporary table s konkértní granularitou
    def data(self, granularity="D"):
        granularities = {"D":"daily","W":"weekly","M":"monthly","Y":"yearly", "T":"trades"}
        if granularities.get(granularity,None)==None:
            raise Exception('Granularity can be only D, W, M, Y or T. Provided value was "{}".'.format(granularity))
        
        table_name = "base_table_{}".format(granularities[granularity])
        if type(getattr(self,table_name)) != pd.DataFrame:
            value = self.__calc_base_table(granularity)
            setattr(self, table_name, value)
        return getattr(self,table_name)
        
    # Napočítání jedné temp tabulky pro konkrétní granularitu
    def __calc_base_table(self, granularity="D"):
        #print(self.roundturns)
        df = self.roundturns[["exit_date","profit","commission","slippage"]].copy()
        df = df.append({"exit_date":datetime.now(),"profit":0,"commission":0,"slippage":0}, ignore_index=True)
        
        if granularity != "T":
            df = df.resample(granularity, on="exit_date").sum()
        df["returns"] = df["profit"] - df["commission"] - df["slippage"]
        df["returns_on_equity_percentual"] = df["returns"]/self.capital
        df["returns_cummulative"] = df["returns"].cumsum()
        df["returns_cummulative_percentual"] = df["returns_cummulative"].pct_change()

        # Drawdown calculation
        df["drawdown"],df["drawdown_on_equity_percentual"] = self.draw_down(df["returns_cummulative"])
        
        return df

    # ----------------------------------------------------------------
    # STATIC FUNCTIONS 
    # The output is a number.
    # ----------------------------------------------------------------
    def summary(self, *args, **kargs):

        columns = [ 
                            {"func":"date_first","args":{}}, 
                            {"func":"date_last","args":{}}, 
                            {"func":"equity_start","args":{}}, 
                            {"func":"equity_max","args":{}}, 
                            {"func":"equity_min","args":{}}, 
                            {"func":"equity_end","args":{}}, 
                            {"func":"max_drawdown","args":{}}, 
                            {"func":"max_drawdown_on_equity_percentual","args":{}},
                            {"func":"profit_factor","args":{}}, 
                            {"func":"annualized_return","args":{}}, 
                            {"func":"gross_profit","args":{}}, 
                            {"func":"gross_loss","args":{}}, 
                            {"func":"return_on_equity","args":{}}, 
                            {"func":"calmar_ratio","args":{}}, 
                            {"func":"sharpe_ratio","args":{}}, 
                            {"func":"total_net_profit","args":{}}, 
                            {"func":"average_yearly_profit","args":{}}, 
                            {"func":"average_monthly_profit","args":{}}, 
                            {"func":"number_of_days","args":{}}, 
                            {"func":"number_of_months","args":{}}, 
                            {"func":"number_of_years","args":{}}
                        ]
        summary = {}
        for column in columns:
            func = column["func"]
            args = column["args"]
            summary[func] = getattr(self, func)(*args)
        return pd.Series(summary)

    # The first value of the equity curve    
    def equity_start(self, *args, **kargs):
        return self.equity_curve().iloc[0]

    # The last value of the equity curve
    def equity_end(self, *args, **kargs):
        return self.equity_curve().iloc[-1]

    # The first value of the equity curve
    def equity_max(self, *args, **kargs):
        return self.equity_curve().max()

    # The lowest value of the equity curve
    def equity_min(self, *args, **kargs):
        return self.equity_curve().min()

    def max_drawdown(self, *args, **kargs):
        return self.data()["drawdown"].min()

    def max_drawdown_on_equity_percentual(self, *args, **kargs):
        return self.data()["drawdown_on_equity_percentual"].min()

    def date_first(self, *args, **kargs):
        return self.roundturns["exit_date"].min()
        
    def date_last(self, *args, **kargs):
        return self.roundturns["exit_date"].max()

    def number_of_days(self, *args, **kargs):
        d1 = self.data().index.min()
        d2 = self.data().index.max()
        return abs((d2 - d1).days)

    def number_of_months(self, *args, **kargs):
        return self.number_of_days()/30
    
    def number_of_years(self, *args, **kargs):
        return self.number_of_months()/12
        
    def total_net_profit(self, *args, **kargs):
        return self.data().iloc[-1]["returns_cummulative"]
        
    def average_yearly_profit(self, *args, **kargs):
        return self.total_net_profit() / self.number_of_years()

    def average_monthly_profit(self, *args, **kargs):
        return self.total_net_profit() / self.number_of_months()

    def gross_profit(self, *args, **kargs):
        return self.roundturns[ self.roundturns["profit"]>0 ]["profit"].sum()

    def gross_loss(self, *args, **kargs):
        return self.roundturns[ self.roundturns["profit"]<0 ]["profit"].sum()

    def profit_factor(self, *args, **kargs): #def winning roundturns profit / losing roundturns loss
        if self.gross_loss() == 0:
            return 0
        else: 
            return abs(self.gross_profit() / self.gross_loss())
        
    def return_on_equity(self, *args, **kargs):
        if self.capital==0:
            return 0
        else:
            return self.total_net_profit() / self.capital

    def annualized_return(self, *args, **kargs) -> float:
        percent = self.return_on_equity()
        months = self.number_of_months()

        if months == 0:
            return percent
        rate = percent / 100
        years = months / 12
        rate = ((rate + 1)**(1 / years)) - 1
        percent = rate * 100
        return percent

    def calmar_ratio(self, *args, **kargs) -> float:
        maxdd = self.max_drawdown_on_equity_percentual()
        if maxdd != 0:
            return self.annualized_return() / self.max_drawdown_on_equity_percentual()*-1
        else:
            return 0

    def sharpe_ratio(self, *args, **kargs):
        # Simulate cumulative returns of 100 days
        #N = 100
        #R = pd.DataFrame(np.random.normal(size=100)).cumsum()
        trades = self.data()
        R = trades["returns_cummulative"]
        # Approach 1
        r = (R - R.shift(1))/R.shift(1)

        # Approach 2
        #r = R.diff()

        sr = r.mean()/r.std() * np.sqrt(252)
        return sr

    # ----------------------------------------------------------------
    #
    #           T I M E   S E R I E S   F U N C T I O N S 
    #
    # The output is a pandas.DataFrame with timeseries index
    # ----------------------------------------------------------------
    # Returns
    # ----------------------------------------------------------------

    # Time series return
    def returns(self, granularity="D", *args, **kargs):
        return self.data(granularity)["returns"]
    
    # Returns against the original capital
    # Usefull for weekly, monthly or yearly returns calculation
    def returns_on_equity_percentual(self, granularity="D", *args, **kargs):
        return self.data(granularity)["returns_on_equity_percentual"]

    def returns_cummulative(self, granularity="D", *args, **kargs):
        return self.data(granularity)["returns_cummulative"]
    
    def returns_cummulative_percentual(self, granularity="D", *args, **kargs):
        return self.data(granularity)["returns_cummulative_percentual"]

    # Drawdown compared against the original capital (only daily granularity)
    def drawdown(self, granularity="D", *args, **kargs):
        drawdown,drawdown_on_equity_percentual = self.draw_down(self.equity_curve(granularity))
        return drawdown

    # Percentual drawdown against the original capital
    # We calculate 'month to month' or 'year to year' drawdown.
    def drawdown_on_equity_percentual(self, granularity="D", *args, **kargs):
        drawdown,drawdown_on_equity_percentual = self.draw_down(self.equity_curve(granularity))
        return drawdown_on_equity_percentual
    
    # Equity curve for presentation in charts
    # New date is added at the beginning of the time series with the initial capital
    def equity_curve(self,granularity="D",*args,**kargs):
        ec = self.data(granularity)["returns_cummulative"]

        first_date = ec.index[0]
        if granularity == "D":
            first_date = ec.index[0] - dateutil.relativedelta.relativedelta(days=1)
        elif granularity == "W":
            first_date = ec.index[0] - dateutil.relativedelta.relativedelta(weeks=1)
        elif granularity == "M":
            first_date = ec.index[0] - dateutil.relativedelta.relativedelta(months=1)
        elif granularity == "Y":
            first_date = ec.index[0] - dateutil.relativedelta.relativedelta(years=1)

        ec.loc[first_date] = 0
        ec = ec + self.capital
        ec = ec.sort_index()
        return ec

    '''
    def trade_by_trade_curve(self,*args,**kargs):
        ec = self.data(granularity)["returns_cummulative"]
        return
    '''

    # ------------------------------------------------------------
    # Method: new_highs
    # ------------------------------------------------------------
    # Time series built on 'equity_curve' with only new high values.
    # The other values are set as NaN.
    # ------------------------------------------------------------
    def new_highs(self, granularity="D", *args, **kargs):
        df = self.equity_curve(granularity)
        df = df.rolling(window=len(df),min_periods=1).max()
        df = df.diff() / df.diff() * df
        # Let us force the first new high as initial capital
        df.iloc[0]=self.capital 
        return df
    
    # ------------------------------------------------------------
    # Method: highest_highs
    # ------------------------------------------------------------
    # Time series build on 'new_highs' series. The NaN values are
    # replaced by last valid.
    # ------------------------------------------------------------
    def highest_highs(self, granularity="D", *args, **kargs):
        return self.new_highs().fillna(method="ffill")


    def get_top_drawdowns(self,top=10):
        drawdowns = pd.DataFrame()
        granularity = "D"
        is_dd = False
        valley = None
        peak = 0
        recovery = None

        counter = 1
        #print(self.data(granularity))
        #equity_curve = self.equity_curve(granularity)
        #print(equity_curve)

        for index,day in self.data(granularity).iterrows():
            dd = day["drawdown"]
            returns = day["equity_curve"]

            # Nejsme v DD
            if is_dd == False and dd==0:
                peak = returns

            # Přechod do DD: Nebyli jsme v DD, ale nyní jsme ... 
            if is_dd == False and dd<0:
                peak_date = index
                valley = dd
                is_dd = True
                last_high = returns

            # Jsme v DD a měříme maximum peak ...
            if is_dd==True and dd<0:
                valley = dd if valley>dd else valley 
            
            # Přechod z DD: Doteďka jsme byli v DD, ale už nejsme ... 
            if (is_dd==True and dd==0) or counter>=len(self.data().index):
                is_dd = False
                recovery_date = index
                drawdowns = drawdowns.append({"peak_date":peak_date,"valley":valley,"recovery_date":recovery_date,"peak":peak}, ignore_index=True)
            
            counter = counter + 1

        drawdowns["recovery_days"] = (drawdowns["recovery_date"] - drawdowns["peak_date"]).dt.days
        drawdowns["valley_on_equity_percentual"] = drawdowns["valley"] / self.capital
        drawdowns["valley_percentual"] = drawdowns["valley"] / drawdowns["peak"]
        return drawdowns.sort_values("valley_on_equity_percentual",ascending=True).head(top)



    # -------------------------------------------------
    # Plotting functions
    # -------------------------------------------------
    
    # PLOT RETURNS DRAWDOWN STUDY
    # --- Todo ---
    # Aplikovat nějaký klouzavý průměr a vyčistit trochu ty křivky
    # Udělat graf s equity trochu větší (gridspec_kw=)
    # Fill bottom chart by a blue color
    def plot_returns_drawdown_study(self,top_dd=10):
        d = self.data(granularity="D")
        dds = self.get_top_drawdowns(top_dd)

        # ALPHA [0-1] -> 0.1-0.25
        dds['alpha'] = (1-(dds['valley_on_equity_percentual'].max() / dds['valley_on_equity_percentual']))*0.10+0.05

        fig, (ax1,ax2) = plt.subplots(2,  sharex=True, gridspec_kw={'hspace': 0, 'wspace': 0}, figsize=(16,8));

        # AXIS 1 .......................................
        ax1.scatter(x=d[ ~d["new_high"].isna() ].index, y=d[ ~d["new_high"].isna() ]["returns_cummulative"], c="limegreen", s=6, zorder=20)
        ax1.plot(d["returns_cummulative"], color="black", zorder=10, linewidth=1)

        lim = ax1.get_ylim()
        for i, (peak_date, recovery_date, alpha) in dds[['peak_date', 'recovery_date', 'alpha']].iterrows():
            if pd.isnull(recovery_date):
                recovery = returns.index[-1]
            
            ax1.fill_between((peak_date, recovery_date),
                            lim[0],
                            lim[1],
                            alpha=alpha,
                            color="grey");

        ax1.set_ylim(lim)
        ax1.set_title('Equity Curve, Top %i drawdown periods' % top_dd);
        ax1.set_ylabel('Equity Curve');
        ax1.legend(['Valley','New High','Top {} Drawdowns'.format(top_dd)], loc='upper left',
                    frameon=False, framealpha=0);
        ax1.set_xlabel('')
        ax1.axhline(y=0, xmin=0, xmax=1, c="silver", linestyle="-", linewidth=1)

        # AXIS 2 .......................................
        #ax2.plot(d["drawdown_on_equity_percentual"]*100, color="black", linewidth=1);
        ax2.fill_between(d.index, d["drawdown_on_equity_percentual"]*100, 0, zorder=100, )

        lim = ax2.get_ylim()
        zorder=1
        for i, (peak_date, recovery_date, alpha) in dds[['peak_date', 'recovery_date','alpha']].iterrows():
            if pd.isnull(recovery_date):
                recovery = returns.index[-1]
            
            ax2.fill_between((peak_date, recovery_date),
                            lim[0],
                            lim[1],
                            alpha=alpha,
                            color="grey",
                            zorder=zorder, );
            zorder = zorder+1
        ax2.set_ylabel('Top {} Drawdowns'.format(top_dd));
        ax2.yaxis.set_major_formatter(mtick.PercentFormatter())
        ax2.axhline(y=0, xmin=0, xmax=1, c="silver", linestyle="-", linewidth=1)
        plt.show()


    # PLOT YEARLY RETURN ON CAPITAL
    def plot_yearly_return_on_equity(self):
        df_returns = self.returns_on_equity_percentual(granularity="Y").to_frame("returns")
        df_returns["date"] = df_returns.index
        df_returns["date"] = df_returns["date"].dt.strftime("%Y")
        df_returns = df_returns.set_index("date")

        fig = plt.figure(figsize=(8,6))
        ax = fig.add_subplot(111)
        #fig, ax = plt.subplots()
        ax.yaxis.set_major_formatter(mtick.PercentFormatter())
        ax.bar(df_returns.index,df_returns["returns"]*100)

        mean = df_returns["returns"].mean()*100
        ax.axhline(mean, xmin=0.05, xmax=0.95, color='#fe6dae', linewidth=2, linestyle=":")
        ax.annotate("Mean: {}%%".format(int(round(mean,0))), (0,mean*1.05), color='#fe6dae')
        #ax.axhline(data2.mean(), color='green', linewidth=2)
        ax.axhline(y=0, xmin=0, xmax=1, c="silver", linestyle="-", linewidth=1)

        plt.xlabel("Years")
        plt.ylabel("Return")
        plt.title("Yearly Return On Initial Capital")
        plt.show()


    # -----------------------------
    # DrawDown Vectorized
    # -----------------------------
    #
    # Input:
    # * series - řada čísel columns=["exit_date","returns"]
    # * capital - počáteční kapitál se kterým máme počítat
    #
    # Returns:
    # * dd_series_abs - řada čísel s absolutním DrawDownem
    # * dd_max_abs - Absolutní hodnota maximálního DrawDownu
    # * dd_series_perc - řada čísel s percentuálním DrawDownem
    # * dd_max_perc - Procentuální hodnota maximálního DrawDownu
    # -----------------------------
    def draw_down(self, nvs: pd.Series, window=None):
        capital = float(self.capital)
        """
        :param nvs: net cumsum value series (equity curve)
        :param window: lookback window, int or None
        if None, look back entire history
        """
        n = len(nvs)
        if window is None:
            window = n
        # rolling peak values
        peak_series = nvs.rolling(window=window, min_periods=1).max()
        """
        Output:
        [0] Draw Down history
        [1] Absolut Draw Down
        [2] Percentual Draw Down History
        [3] Percentual Draw Down
        """
        dd_series_abs = nvs-peak_series
        dd_series_perc = ((nvs-peak_series)/capital) if capital!=0 else 0
        return dd_series_abs, dd_series_perc

    '''
    def get_monte_carlo(systems, capital, start_date, end_date):
        global trades
        data = trades.copy()
        capital = float(capital)

        if not isinstance(systems, list):
            systems = [systems]
            return

        if start_date != None:
            data = data[data["CloseDateTime"] >= start_date]
        if end_date != None:
            data = data[data["CloseDateTime"] <= end_date]

        if len(data) == 0:
            return {}

        data = data[data["System"].isin(systems)]["NetProfit"].tolist()
        mc = ta.MonteCarlo(data, iterations=10000, capital=capital)
        output = mc.data()
        output["Systems"] = systems
        return output

    
    def get_correlation_map(systems,period="%Y%W"):
        global trades

        if not isinstance(systems, list):
            systems = list(systems)

        #systems = ["CL"]
        data = trades.copy()

        # Finding min & max date
        dates = data[ data["System"].isin(systems) ].groupby("System").agg({"CloseDateTime":["min","max"]})
        #print(dates)
        date_start = dates["CloseDateTime"]["min"].max()
        date_end = dates["CloseDateTime"]["max"].min()
        #print(date_start,date_end)
        #print()

        # Weekly aggregation of changes for selected strategies
        df = data[ data["System"].isin(systems) ].copy()
        df["NetProfit"] = df["Profit"] - df["CommissionRT"] - df["SlippageTotal"]
        df = df[ df["CloseDateTime"]>=date_start ]
        df = df[ df["CloseDateTime"]<=date_end ]
        df = df.groupby(['System', df['CloseDateTime'].dt.strftime(period)]).agg({"Profit":"sum","NetProfit":"sum"})
        df["equity"] = df["NetProfit"].cumsum()
        df["change"] = df["equity"].pct_change()
        df = df.reset_index()
        df = df.pivot(index="CloseDateTime", columns="System", values="change")
        df = df.fillna("0")
        df = df.astype(float)

        # Spočítáme korelační tabulku
        corr = df.corr()
        corr.columns.name = None
        corr.index.name = None
        #print(corr)
        return corr
    '''