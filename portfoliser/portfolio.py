import pandas as pd
from .utils import *
from .returns import *
from .roundturns import *

'''
roundturns = pd.DataFrame with trades
capital = int
multiplier = int or dict("strategy1":1,"strategy2":1,"strategy3":3)
monthly_fee = int or dict()
commission = int or dict("strategy1":20,"strategy2":10,"strategy3":7)
'''


class portfolio:
    def __init__(   self,
                    roundturns = None, # Seznam roundturns (uzavřených obchodů)
                    capital = 100000,
                    multiplier = 1,
                    monthly_fee = 0,
                    days_back = None,
                    ):
        if type(roundturns) != pd.DataFrame:
            raise Exception('Attempt to create trade class without roundturns data.')
        self.roundturns = validate_roundturns_data( roundturns )
        self.capital = capital
        self.multiplier = validate_strategy_parameters(roundturns,multiplier,"multiplier")
        self.monthly_fee = validate_strategy_parameters(roundturns,monthly_fee,"monthly_fee")

        if days_back != None:
            self.days_back = days_back
            d = datetime.today() - timedelta(days=self.days_back)
            self.roundturns = self.roundturns[ self.roundturns["exit_date"] >= d ]
        return
    
    # Public method returning the return portfolio
    def returns(    self,
                    func, 
                    totals=True, 
                    drilldown=True, 
                    granularity="Y",
                    dimension_column="strategy", 
                    days_back=None):
        return self.__cycle(returns,func, totals, drilldown, granularity, dimension_column, days_back)

    # Public method returning the trade statistics of portfolio
    def trades(     self, 
                    func, 
                    totals=True, 
                    drilldown=True, 
                    granularity="Y",
                    dimension_column="strategy", 
                    days_back=None):
        return self.__cycle(trades,func, totals, drilldown, granularity,dimension_column)

    # Public method returning the whole summary of portfolio
    def summary(self,*args,**kargs):
        returns = self.returns("summary",*args,**kargs)
        trades = self.trades("summary",*args,**kargs)
        summary = returns.append(trades)
        return summary

    # Private method for cycling through all the strategies included in portfolio 
    def __cycle(    self,
                    package, 
                    func, 
                    totals=True, 
                    drilldown=True, 
                    granularity="D", 
                    dimension_column="strategy", 
                    days_back=None):
        if dimension_column not in self.roundturns.columns:
            raise Exception("Dimension '{}' is not valid column in data.".format(dimension_column))
        if self.roundturns[dimension_column].dtype != "object":
            raise Exception("Column '{}' containing dimension has invalid type {}. Dimension needs to be string ('object').".format(dimension_column,self.roundturns.info()[dimension_column]))
        df = pd.DataFrame()
        dimension_list = list(self.roundturns[dimension_column].unique())
        dimension_list.sort()

        args = {"granularity": granularity}

        # Drilldown - strategies
        if drilldown==True:
            for dimension_value in dimension_list:
                r = package(    self.roundturns[ self.roundturns[dimension_column]==dimension_value], 
                                capital=self.capital, 
                                multiplier=self.multiplier[dimension_value],
                                monthly_fee=self.monthly_fee[dimension_value])
                df_new  = getattr(r,func)(**args)

                if type(df_new)!=pd.Series and type(df_new)!=pd.DataFrame:
                    df_new = pd.Series(df_new)

                if type(df_new)==pd.Series:
                    df_new = pd.DataFrame(df_new)

                df_new.columns = [dimension_value]
                if df.empty:
                    df = df_new
                else:
                    df = df.merge(df_new, how="outer", left_index=True, right_index=True)
        # Totals
        if totals==True:
            r = package(self.roundturns, capital=self.capital)
            df_new  = getattr(r,func)(**args)
            if type(df_new)!=pd.Series and type(df_new)!=pd.DataFrame:
                df_new = pd.Series(df_new)
            if type(df_new)==pd.Series:
                df_new = pd.DataFrame(df_new)
            df_new.columns = ["all"]
            if df.empty:
                df = df_new
            else:
                df = df.merge(df_new, how="outer", left_index=True, right_index=True)

        return df
    
