def add_to_statistics(name, value, type):

    if isinstance(value, tuple):
        return [name,
                self.format_statistics(value[0], type),
                self.format_statistics(value[1], type),
                self.format_statistics(value[2], type)]
    else:
        return [name,
                self.format_statistics(value, type),
                "",
                ""]

def format_statistics(value, type):
    if type == "$":
        return "${:,.0f}".format(value).replace(",", " ")
    elif type == "%":
        return "{:,.0f}%".format(value * 100).replace(",", " ")
    elif type == "0":
        return "{:,.0f}".format(value).replace(",", " ")
    elif type == "0.00":
        return "{:,.2f}".format(value).replace(",", " ")
    return value

def get_financial_statistics():
    data = [self.add_to_statistics("Total Net Profit (Profit - Commissions - Developer Fee)", self.total_net_profit(), "$"),
            #self.add_to_statistics("Total Gross Profit", self.gross_profit(), "$"),
            #self.add_to_statistics("Standard Deviation", self.standard_deviation(), "0.00"),
            self.add_to_statistics("Gross Profit (Winning Trades)", self.gross_profit(), "$"),
            self.add_to_statistics("Gross Loss (Losing Trades)", self.gross_loss(), "$"),
            self.add_to_statistics("Profit Factor", self.profit_factors(), "0.00"),
            #self.add_to_statistics("Average Monthly Return", self.average_monthly_return(), "0.00"),
            #self.add_to_statistics("Std. Deviation of Monthly Return", self.standard_deviation_of_monthly_return(),"0.00"),
            #self.add_to_statistics("Sharpe Ratio",self.sharpe_ratio(),""),
            self.add_to_statistics("Return On Equity", self.return_on_equity(), "%"),
            self.add_to_statistics("Annualized Return",self.annualize_return(),"%"),
            self.add_to_statistics("Maximum Drawdown", self.max_drawdown_abs(), "$"),
            self.add_to_statistics("Maximum Drawdown [%]", self.max_drawdown_perc(), "%"),
            ]
    df = pd.DataFrame(columns=["Metric", "All Trades", "Long Trades", "Short Trades"], data=data)
    return df

def get_trades_statistics():
    data = [
            self.add_to_statistics("Total Number of Trades", self.total_number_of_trades(), "0"),
            self.add_to_statistics("Percent Profitable", self.percent_profitable(), "%"),
            self.add_to_statistics("Winning Trades", self.number_of_winning_trades(), "0"),
            self.add_to_statistics("Losing Trades", self.number_of_losing_trades(), "0"),
            self.add_to_statistics("Largest Winning Trade", self.largest_winning_trade(), "$"),
            self.add_to_statistics("Largest Losing Trade", self.largest_losing_trade(), "$"),
            self.add_to_statistics("Average Winning Trade", self.average_winning_trade(), "$"),
            self.add_to_statistics("Average Losing Trade", self.average_losing_trade(), "$"),
            self.add_to_statistics("Ratio Avg. Win / Avg. Loss", self.average_win_loss_ratio(), "0.00"),
            self.add_to_statistics("Average Time Held [Days]",self.average_time_held(),""),
            ]
    df = pd.DataFrame(columns=["Metric", "All Trades", "Long Trades", "Short Trades"], data=data)
    return df